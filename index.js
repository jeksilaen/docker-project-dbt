const express = require('express');
const redis = require('redis');

const app = express();
const client = redis.createClient({
    url: 'redis://redis:6379'
    // port: 6379
})

async function go(){
    await client.connect()
    await client.set('counter', 0)

    app.get('/', async(req, res) => {
        const response = await client.get('counter')
        await client.set('counter', parseInt(response) + 1)

        res.send("Page visitor counter : " + response)
    })
}
go()

app.listen(4001, (req, res) => {
    console.log("Server listening on port 4001");
})